create database manuinfo;

use manuinfo;

create table cadastro(
usuario varchar(30),
nome varchar(60),
endereco varchar(60),
nascimento date,
senha varchar(60),
telefone varchar(30),
cpf int(30), 
sexo char,
email varchar(90),
primary key(usuario)
);

create table servicos(
codPedido int,
aparelho varchar(20),
modelo varchar(20),
problema varchar(200),
atendimento varchar(10),
frete varchar(100),
endereco varchar(100),
cep int,
numero int,
valortotal varchar(100),
observacao varchar(100),
primary key(codPedido),
foreign key (usuario)references cadastro(usuario)
);

create table funcionario(
funcionario varchar(40),
senha int,
primary key(funcionario)
);